## 0.1.1 (2023-04-30)
### fix
- [[`cfde93d`](https://gitlab.com/katalytic/katalytic-checks/commit/cfde93d73b9f1f7f142168bf4743ba24d8819305)] missing __version__


## 0.1.0 (2023-04-30)
### feat
- [[`0e81081`](https://gitlab.com/katalytic/katalytic-checks/commit/0e8108157478cc87e2f06724109136bf928a0d50)] add is_number()
### fix
- [[`eeb80c0`](https://gitlab.com/katalytic/katalytic-checks/commit/eeb80c090e39e8e4ee292be075af47c4f32150f0)] is_generator(), is_iterable(), is_iterable_or_str(), is_iterator(), is_primitive() and use all_types/all_types_besides for test parametrization


## 0.0.1 (2023-04-27)
### fix
- [[`1c6b9b6`](https://gitlab.com/katalytic/katalytic-checks/commit/1c6b9b6f9fca9cde0b985fd0660c8030c6aafa1d)] remove wrong info from README.md

